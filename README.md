# Creature Statblock Creator
Note: All stat blocks are made in likelihood to Wizards of the Coasts' beautiful
templates. I do no claim liability, responsibility, or any share over the
creations that any person may create from the usage of my forms which 
automatically fills the template graciously provided by [Valloric](https://valloric.github.io/statblock5e/).
I also do not claim any of Valloric's code as my own, I simply wish to use my
product to make his template easily editable and more accessable to the public.
Should there be questions, concerns, notices, or issues, please notify me via 
email: hextaine@gmail.com.

## TODO List:
* change updates to be less "costly" (don't re-fetch values when given values)
* Saving Throws
* Skills
* Armor types
* Legendary Actions
    - Adding these to a second column? See Valloric's page to get more
    info on this
* CR (Is this even a thing that I should include?)



version 1.0