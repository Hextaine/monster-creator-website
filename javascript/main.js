let baseValues = {
    input_name: "Animated Armor",
    input_size: "Medium",
    input_type: "construct",
    input_alignment: "unaligned",
    input_ac: "18",
    input_hp: "33",
    input_num_hp_dice: "6",
    input_hp_dice: "d8",
    input_hp_mod: "6",
    input_speed: "25",
    input_speed_fly: "0",
    input_speed_swim: "0",
    str: "14",
    dex: "11",
    con: "13",
    int: "1",
    wis: "3",
    cha: "1",
    dmg_immunities: "poison, psychic",
    condition_immunities: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, petrified, poisoned",
    input_senses: "blindsight 60 ft. (blind beyond this radius), passive Perception 6",
    input_languages: "-",
    attributes: 
            [{
                name: "Antimagic Susceptibility. ",
                description: "The armor is incapacitated while in the area of an /antimagic field/.  If targeted by /dispel magic/, the armor must succeed on a Constitution saving throw against the caster’s spell save DC or fall unconscious for 1 minute. "
            },
            {
                name: "False Appearance. ",
                description: "While the armor remains motionless, it is indistinguishable from a normal suit of armor."
            }],
    actions: 
            [{
                name: "Multiattack. ",
                description: "The armor makes two melee attacks. "
            },
            {
                name: "Slam. ",
                description: "/Melee Weapon Attack:/ +4 to hit, reach 5 ft., one target. /Hit:/ 5 (1d6 + 2) bludgeoning damage."
            }],
    legendaries:
            [{
                name: "",
                description: "The tarrasque can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The tarrasque regains spent legendary actions at the start of it's turn."
            },
            {
                name: "Attack. ",
                description: "The tarrasque makes one claw attack or tail attack."
            },
            {
                name: "Move. ",
                description: "The tarrasque moves up to half its speed."
            },
            {
                name: "Chomp (Costs 2 Actions). ",
                description: "The tarrasque makes one bite attack or uses its /Swallow/."
            }]
}
baseValues = baseValues;

let disabledStatElements = {
    attributes: false
};

/*** Helper Functions ***/
function sanitizeInput(str) {
    if (typeof str !== 'string')
        return str;
    return str.replace(/</g, "&lt;").replace(/>/g, "&gt;");
}

function resolveTextFormatting(str) {
    let formatted = [];

    let italic = -1;
    for (let i = str.length - 1; i >= 0; i--) {
        formatted.unshift(str.charAt(i));

        if (italic >= 0)
            italic++;

        if (str[i] === '/' && (i === 0 || str[i - 1] !== '\\')) {
            if (italic < 0)
                italic = 0;
            else {
                formatted.splice(italic, 1, '</i>');
                formatted[0] = '<i>';
                italic = -1;
            }
        }
    }

    return formatted.join('');
}

function replaceAll(str, old, repl) {
    while (str.includes(old)) {
        str = str.replace(old, repl);
    }

    return str;
}

function isEmpty(obj) {
    if (!exists(obj))
        return true;
    if (typeof obj === 'string' && obj.trim() === '')
        return true;
    if (typeof obj === 'object' && obj.value === '' && obj.innerHTML === '')
        return true;
    return false;
}

function exists(obj) {
    if (obj === undefined || obj === null)
        return false;
    return true;
}

function autoGrow(element) {
    if (element.scrollHeight > element.clientHeight)
        element.style.height = element.scrollHeight + "px";
}

/*** Functional Code ***/
(function() {
    if (isEmpty(sessionStorage.getItem('disabledStatElements')))
        disabledStatElements = new Object();
    else
        disabledStatElements = JSON.parse(sessionStorage.getItem('disabledStatElements'));

    loadInputs();
}());

function loadInputs() {
    let allInputs = document.getElementsByClassName('creature_input');

    for (let i = 0; i < allInputs.length; i++) {
        loadSavedValues(allInputs[i], '');
        updateStatBlock(allInputs[i]);
    }

    // TODO: Skipped Boxes cause later inputs to be ignored

    let variableTextInputs = ['dmg-immunities', 'condition-immunities'];
    variableTextInputs.forEach(varInput => {
        for (let numInputs = 0; !isEmpty(sessionStorage.getItem(`${varInput}-${numInputs}`)); numInputs++) {
            let savedTextInput = addTextInput(document.getElementById(`add_${varInput}`));
            savedTextInput.value = sessionStorage.getItem(`${varInput}-${numInputs}`);
        }

        updateStatBlock(document.getElementById(`input_${varInput}`))
    });

    let variableTitledInputs = ['attributes', 'actions', 'legendaries'];
    variableTitledInputs.forEach(varInput => {
        for (let i = 0; !isEmpty(sessionStorage.getItem(`${varInput}-${i}-name`)) || 
                        !isEmpty(sessionStorage.getItem(`${varInput}-${i}-description`)); i++) {
            let titledInput = addTitledInput(document.getElementById(`add_${varInput}`));
            titledInput.children[0].value = sessionStorage.getItem(`${varInput}-${i}-name`);
            titledInput.children[1].value = sessionStorage.getItem(`${varInput}-${i}-description`);
            updateStatBlock(titledInput);
        }
        
        updateStatBlock(document.getElementById(`input_${varInput}`))
    });
}

function saveValue(element) {
    sessionStorage.setItem(element.id, element.value);
    updateStatBlock(element);
}

function loadSavedValues(element, baseValue) {
    if (element === undefined) return baseValue;
    if (isEmpty(sessionStorage.getItem(element.id))) {
        return baseValue;
    }

    element.value = sessionStorage.getItem(element.id);

    return sessionStorage.getItem(element.id);
}

function updateStatBlock(element) {
    if (!exists(element))
        return;

    let inputId = (typeof element === 'string') ? element : element.id;
    let statId = inputId.substring(inputId.indexOf('_') + 1);

    let numberedElement = /[.]*-[0-9]+/i;
    if (numberedElement.test(statId)) {
        statId = statId.substring(0, statId.indexOf(numberedElement.exec(inputId)));
    }

    if (disabledStatElements[statId] === true) {
        hideStat(statId);
        return;
    }

    switch (statId) {
        case 'hp':
        case 'num-hp-dice':
        case 'hp-dice':
        case 'hp-mod':
            updateHP();
            return;
        case 'speed':
        case 'speed-fly':
        case 'speed-swim':
            updateSpeed();
            return;
        case 'str':
        case 'dex':
        case 'con':
        case 'int':
        case 'wis':
        case 'cha':
            updateAbility(statId, sanitizeInput(element.value.trim()));
            return;
        case 'dmg-immunities':
        case 'condition-immunities':
            updateListDisplay(statId);
            return;
        case 'attributes':
        case 'actions':
        case 'legendaries':
            updateAttributesAndActions(statId);
            return;
    }

    let statElement = document.getElementById(statId);
    if (isEmpty(statElement)) {
        return;
    }

    let sanitizedValue = sanitizeInput(element.value.trim());
    if (sanitizedValue === '') {
        sanitizedValue = fetchOrDefault(inputId);
    }
    statElement.innerHTML = sanitizedValue;

    return;
}

function fetchOrDefault(id) {
    if (disabledStatElements[id] === true)
        return '';

    let sanitizedId = id;
    sanitizedId = replaceAll(sanitizedId, '-', '_');

    if (isEmpty(baseValues[sanitizedId]))
        return '-';    

    if (isEmpty(document.getElementById(id)))
        return baseValues[sanitizedId];

    return sanitizeInput(document.getElementById(id).value);
}

function updateHP() {
    let hpBlock = document.getElementById('hp-block');
    let hp = fetchOrDefault('input_hp');
    let hpDice = fetchOrDefault('input_hp-dice');
    let numHpDice = fetchOrDefault('input_num-hp-dice');
    let hpMod = fetchOrDefault('input_hp-mod');

    // TODO: Somehow make sure dice & HP match up????

    if (hpMod > 0)
        hpBlock.innerHTML = `${hp} (${numHpDice}${hpDice} +${hpMod})`
    else 
        hpBlock.innerHTML = `${hp} (${numHpDice}${hpDice} -${hpMod})`
}

function updateSpeed() {
    let speedBlock = document.getElementById('speed-block');
    let speed = fetchOrDefault('input_speed');
    let speed_fly = fetchOrDefault('input_speed-fly');
    let speed_swim = fetchOrDefault('input_speed-swim');

    let speedsDisplayed = `${speed}ft.`;
    if (speed_fly > 0)
        speedsDisplayed = `${speedsDisplayed}, fly ${speed_fly}ft.`;
    if (speed_swim > 0)
        speedsDisplayed = `${speedsDisplayed}, swim ${speed_swim}ft.`;

    speedBlock.innerHTML = speedsDisplayed;
}

function updateAbility(id, value) {
    let stripped_id = id.replace('input_', '');
    let statElement = document.getElementsByTagName('abilities-block')[0]['attributes'][`data-${stripped_id}`];

    if (isEmpty(statElement))
        return;

    if (value === undefined || value === '')
        value = fetchOrDefault(id);
    statElement.value = value;
    document.getElementsByTagName('abilities-block')[0].attachedCallback();
}

function updateListDisplay(listId) {
    let statElement = document.getElementById(listId);
    let listInputs = document.getElementsByClassName(listId);

    let formattedList = '';
    for (let i = 0; i < listInputs.length; i++) {
        if (formattedList === '')
            formattedList = `${sanitizeInput(listInputs[i].value)}`;
        else if (listInputs[i].value != '')
            formattedList = `${formattedList}, ${sanitizeInput(listInputs[i].value)}`
    }

    if (isEmpty(formattedList) || formattedList.trim() === ',')
        statElement.innerHTML = baseValues[`${replaceAll(listId, '-', '_')}`];
    else
        statElement.innerHTML = formattedList;
}

function updateAttributesAndActions(groupId) {
    if (disabledStatElements[groupId] === true)
        return;

    let statElement = document.getElementById(`${groupId}`);

    for (let i = statElement.children.length - 1; i >= 0; i--) {
        if (statElement.children[i].tagName === 'PROPERTY-BLOCK')
            statElement.removeChild(statElement.children[i]);
    }

    let propertyObjects = [];

    let index = 0;
    let name = document.getElementById(`${groupId}-${index}-name`);
    let description = document.getElementById(`${groupId}-${index}-description`);
    while (exists(name) && exists(description)) {
        let property = {
            name: '',
            description: ''
        };
        if (name.value !== '')
            property.name = `${sanitizeInput(name.value)}. `;
        property.description = `${sanitizeInput(description.value)}`;

        if (property.name !== '' || property.description !== '')
            propertyObjects.push(property);

        index++;
        name = document.getElementById(`${groupId}-${index}-name`);
        description = document.getElementById(`${groupId}-${index}-description`);
    }

    if (propertyObjects.length === 0)
        propertyObjects = baseValues[groupId];

    loadPropertyBlocks(propertyObjects).forEach(block => {
        statElement.appendChild(block);
    });
}

function loadPropertyBlocks(propObjList) {
    let propertyBlocks = [];
    
    propObjList.forEach(propObj => {
        let propertyBlock = document.createElement('property-block');
        let name = document.createElement('h4');
        name.innerHTML = propObj.name;

        let description = document.createElement('p');
        description.innerHTML = resolveTextFormatting(propObj.description);

        propertyBlock.appendChild(name);
        propertyBlock.appendChild(description);

        propertyBlocks.push(propertyBlock);
    });

    return propertyBlocks;
}

function addTextInput(element) {
    let idGroup = element.id.substring(element.id.indexOf('_') + 1);
    let parentList = document.getElementById(`input_${idGroup}`);
    let numInput = parentList.children.length - 3;

    let listItemToAdd = document.createElement('li');
    let inputToAdd = document.createElement('input');
    inputToAdd.id = `${idGroup}-${numInput}`;
    inputToAdd.type = 'text';
    inputToAdd.classList.add(`${idGroup}`);
    inputToAdd.classList.add('creature_input');
    inputToAdd.addEventListener('keyup', function(event) {
        saveValue(inputToAdd);
    });

    let removeButton = document.createElement('button');
    removeButton.id = `remove_${inputToAdd.id}`;
    removeButton.type = 'button';
    removeButton.innerHTML = 'Remove';
    removeButton.classList.add('remove');
    removeButton.addEventListener('click', function(event) {
        removeInput(inputToAdd);
    });

    listItemToAdd.appendChild(inputToAdd);
    listItemToAdd.appendChild(removeButton);

    parentList.insertBefore(listItemToAdd, parentList.children[parentList.children.length - 3]);

    return inputToAdd;
}

function addTitledInput(element) {
    let groupId = element.id.split('_')[1];
    let parentList = document.getElementById(`input_${groupId}`);
    let listItemToAdd = document.createElement('li');
    let numInput = parentList.children.length - 3;
    
    let titleToAdd = document.createElement(`input`);
    titleToAdd.id = `${groupId}-${numInput}-name`;
    titleToAdd.type = 'text';
    titleToAdd.classList.add('titledInput');
    titleToAdd.classList.add(`${groupId}`);
    titleToAdd.classList.add('creature_input');
    titleToAdd.placeholder = `${groupId.charAt(0).toUpperCase()}${groupId.substring(1, groupId.length - 1)} Title`;
    titleToAdd.addEventListener('keyup', function(event) {
        saveValue(titleToAdd);
    });
    listItemToAdd.appendChild(titleToAdd);

    let textAreaToAdd = document.createElement('textarea');
    textAreaToAdd.id = `${groupId}-${numInput}-description`;
    textAreaToAdd.classList.add(`${groupId}`);
    textAreaToAdd.classList.add('creature_input');
    textAreaToAdd.placeholder = `${groupId.charAt(0).toUpperCase()}${groupId.substring(1, groupId.length - 1)} description`
    textAreaToAdd.rows = "2";
    textAreaToAdd.cols = "30";
    textAreaToAdd.addEventListener('input', function(event) {
        saveValue(textAreaToAdd);
        autoGrow(this);
    });
    listItemToAdd.appendChild(textAreaToAdd);

    let removeButton = document.createElement('button');
    removeButton.id = `remove_${groupId}-${numInput}`;
    removeButton.type = 'button';
    removeButton.innerHTML = 'Remove';
    removeButton.classList.add('remove');
    removeButton.classList.add('top-align');
    removeButton.addEventListener('click', function(event) {
        removeInput(titleToAdd);
    });
    listItemToAdd.appendChild(removeButton);

    parentList.insertBefore(listItemToAdd, parentList.children[parentList.children.length - 3]);

    return listItemToAdd;
}

function disableInputGroup(element) {
    let groupId = element.id.split('_')[1];
    hideStat(groupId);
}

function enableInputGroup(element) {
    let groupId = element.id.split('_')[1];
    showStat(groupId);
}

function toggleVisibilityForStat(groupId) {
    if (disableInputGroup[statId])
        hideStat(groupId);
    else
        showStat(groupId);
}

function hideStat(groupId) {
    disabledStatElements[groupId] = true;
    sessionStorage.setItem('disabledStatElements', JSON.stringify(disabledStatElements));
    document.getElementById(groupId).parentElement.style.display = 'none';

    if (exists(document.getElementById(`disable_${groupId}`))) {
        document.getElementById(`disable_${groupId}`).style.display = 'none';
        document.getElementById(`enable_${groupId}`).style.display = 'inline-block';
    }
}

function showStat(groupId) {
    disabledStatElements[groupId] = false;
    sessionStorage.setItem('disabledStatElements', JSON.stringify(disabledStatElements));
    document.getElementById(groupId).parentElement.style.display = 'inline-block';

    updateStatBlock(document.getElementById(`input_${groupId}`));
    
    if (exists(document.getElementById(`enable_${groupId}`))) {
        document.getElementById(`enable_${groupId}`).style.display = 'none';
        document.getElementById(`disable_${groupId}`).style.display = 'inline-block';
    }
}

function removeInput(element) {
    let groupId = element.id.split(/-[0-9]+/g)[0];
    let parentList = document.getElementById(`input_${groupId}`);

    let parseForNum = /[0-9]+/g;
    let removedIndex = parseForNum.exec(element.id)[0];

    if (removedIndex === '0') {
        for (let numInput = 0; numInput < parentList.children[0].children.length; numInput++) {
            sessionStorage.removeItem(`${parentList.children[0].children[numInput].id}`)
        }
    }

    parentList.removeChild(parentList.children[removedIndex]);

    let listIndex = parseInt(removedIndex);
    console.log(removedIndex);
    for (; parentList.children[listIndex].tagName === 'LI'; listIndex++) {
        let inputGroup = parentList.children[listIndex]; 
        if (parseForNum.exec(inputGroup.firstChild.id) === `${listIndex}`)
            continue;
        
        for (let numInput = 0; numInput < inputGroup.children.length && 
                               inputGroup.children[numInput].tagName !== 'BUTTON'; numInput++) {
            if (parentList.children[listIndex + 1].tagName !== 'LI') {
                sessionStorage.removeItem(`${inputGroup.children[numInput].id}`)
            }

            let suffix = inputGroup.children[numInput].id.split(/[0-9]+/)[1];
            inputGroup.children[numInput].id = `${groupId}-${listIndex}${suffix}`;

            sessionStorage.setItem(`${inputGroup.children[numInput].id}`, `${inputGroup.children[numInput].value}`);
        }
    }



    updateStatBlock(parentList);
}